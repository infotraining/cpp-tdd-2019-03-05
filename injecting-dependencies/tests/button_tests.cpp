#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "button.hpp"

using namespace std;

namespace InterfaceInjection
{
    struct SwitchMock : Switch
    {
        MOCK_METHOD0(on, void());
        MOCK_METHOD0(off, void());
    };

    class ButtonTests : public ::testing::Test
    {
    protected:
        SwitchMock mq_switch;
        Button btn;

        ButtonTests() : btn{mq_switch}
        {}
    };

    struct Button_IsOffState : ButtonTests
    {};

    TEST_F(Button_IsOffState, WhenClickedLightIsOn)
    {
        EXPECT_CALL(mq_switch, on()).Times(1);

        btn.click();
    }

    struct Button_IsOnState : ButtonTests
    {

        void SetUp() override
        {
            EXPECT_CALL(mq_switch, on()).Times(1);
            btn.click();
        }
    };

    TEST_F(Button_IsOnState, WhenClickedLightIsOff)
    {
        EXPECT_CALL(mq_switch, off()).Times(1);
        btn.click();
    }
}

namespace TemplateParameterInjection
{
    struct SwitchMock
    {
        MOCK_METHOD0(on, void());
        MOCK_METHOD0(off, void());
    };

    class ButtonTests_TI : public ::testing::Test
    {
    protected:
        SwitchMock mq_switch;
        Button<SwitchMock> btn;

        ButtonTests_TI() : btn{mq_switch}
        {}
    };

    struct Button_IsOffState_TI : ButtonTests_TI
    {};

    TEST_F(Button_IsOffState_TI, WhenClickedLightIsOn)
    {
        EXPECT_CALL(mq_switch, on()).Times(1);

        btn.click();
    }

    struct Button_IsOnState_TI : ButtonTests_TI
    {

        void SetUp() override
        {
            EXPECT_CALL(mq_switch, on()).Times(1);
            btn.click();
        }
    };

    TEST_F(Button_IsOnState_TI, WhenClickedLightIsOff)
    {
        EXPECT_CALL(mq_switch, off()).Times(1);
        btn.click();
    }
}