#include "button.hpp"
#include "led_switch.hpp"
#include <iostream>
#include <boost/di.hpp>

using namespace std;

int main()
{
	LEDSwitch led_switch;
	// InterfaceInjection::Button btn{led_switch};

	auto injector = boost::di::make_injector(
		boost::di::bind<InterfaceInjection::Switch>().to(led_switch),
		boost::di::bind<class TemplateParameterInjection::SwitchType>().to<LEDSwitch>()
	);

	//auto btn = injector.create<InterfaceInjection::Button>();
	auto btn = injector.create<TemplateParameterInjection::Button>();

	btn.click();
	btn.click();
	btn.click();
	btn.click();
	btn.click();
	btn.click();

	return 0;
}