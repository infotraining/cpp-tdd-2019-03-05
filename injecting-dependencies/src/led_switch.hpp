#ifndef LIGHT_SWITCH_HPP
#define LIGHT_SWITCH_HPP

#include "button.hpp"
#include "led_light.hpp"

class LEDSwitch : public InterfaceInjection::Switch
{
    LEDLight light_;

public:
    void on() override
    {
        light_.set_rgb(255, 255, 255);
    }

    void off() override
    {
        light_.set_rgb(0, 0, 0);
    }
};

#endif