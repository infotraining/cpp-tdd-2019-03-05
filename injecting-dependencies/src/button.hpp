#ifndef BUTTON_HPP
#define BUTTON_HPP

#include "led_light.hpp"

namespace InterfaceInjection
{
    class Switch
    {
    public:
        virtual void on() = 0;
        virtual void off() = 0;
        virtual ~Switch() = default;
    };

    class Button
    {
        Switch& light_switch_;
        bool is_on_;

    public:
        Button(Switch& light_switch)
            : light_switch_{light_switch}
            , is_on_{false}
        {
        }

        void click()
        {
            if (!is_on_)
            {
                light_switch_.on();
                is_on_ = true;
            }
            else
            {
                light_switch_.off();
                is_on_ = false;
            }
        }
    };
}

namespace TemplateParameterInjection
{
    class SwitchType;

    template <typename TSwitch = class SwitchType>
    class Button
    {
        TSwitch& light_switch_;
        bool is_on_;
    public:
        Button(TSwitch& light_switch)
            : light_switch_{light_switch}
            , is_on_{false}
        {
        }

        #ifdef TESTING_MODE
        TSwitch& get_switch()
        {
            return light_switch;
        }
        #endif

        void click()
        {
            if (!is_on_)
            {
                light_switch_.on();
                is_on_ = true;
            }
            else
            {
                light_switch_.off();
                is_on_ = false;
            }
        }
    };
}

#endif