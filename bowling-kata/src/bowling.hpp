#ifndef BOWLING_HPP
#define BOWLING_HPP

#include <vector>
#include <string>
#include <algorithm>
#include <array>

class BowlingGame
{
    std::array<unsigned int, 21> pins_ = {};
    unsigned int roll_no_ = 0;

public:
    static constexpr unsigned int pins_in_frame = 10;
    static constexpr unsigned int no_of_frames = 10;

    BowlingGame();

    unsigned int score() const
    {
        unsigned int result{};
        unsigned int roll_index = 0;

        for (unsigned int i = 0; i < no_of_frames; ++i)
        {
            if (is_strike(roll_index))
            {
                result += pins_in_frame
                    + strike_bonus(roll_index);
                roll_index += 1;
            }
            else
            {
                result += frame_score(roll_index);

                if (is_spare(roll_index))
                    result += spare_bonus(roll_index);

                roll_index += 2;
            }
        }

        return result;
    }

    unsigned int frame_score(unsigned int roll_index) const
    {
        return pins_[roll_index] + pins_[roll_index + 1];
    }

    bool is_strike(unsigned int roll_index) const
    {
        return pins_[roll_index] == pins_in_frame;
    }

    unsigned int strike_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + 1] + pins_[roll_index + 2];
    }

    bool is_spare(unsigned int roll_index) const
    {
        return frame_score(roll_index) == pins_in_frame;
    }

    unsigned int spare_bonus(unsigned int roll_index) const
    {
        return pins_[roll_index + 2];
    }

    void roll(unsigned int pins)
    {
        pins_[roll_no_] = pins;
        ++roll_no_;
    }
};

#endif