#include "bowling.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <array>

using namespace std;

class BowlingGameTests : public ::testing::Test
{
protected:
    BowlingGame game;

    void roll_many(unsigned int count, unsigned int pins)
    {
        for (unsigned int i = 0; i < count; ++i)
            game.roll(pins);
    }

    void roll_spare()
    {
        game.roll(1);
        game.roll(9);
    }

    void roll_strike()
    {
        game.roll(BowlingGame::pins_in_frame);
    }
};

TEST_F(BowlingGameTests, NewGameScoreIsZero)
{
    auto score = game.score();

    ASSERT_EQ(score, 0);
}

TEST_F(BowlingGameTests, AllRollsInGutterScoreIsZero)
{
    roll_many(20, 0);

    ASSERT_EQ(game.score(), 0);
}

TEST_F(BowlingGameTests, WhenAllRollsNoMarkScoreIsSumOfPins)
{
    roll_many(20, 2);

    ASSERT_EQ(game.score(), 40);
}

TEST_F(BowlingGameTests, WhenSpareNextRollIsCountedTwice)
{
    roll_spare();

    roll_many(18, 1);

    ASSERT_EQ(game.score(), 29);
}

TEST_F(BowlingGameTests, WhenStrikeNextTwoRollsAreCountedTwice)
{
    roll_many(2, 1);
    roll_strike();
    roll_many(16, 1);

    ASSERT_EQ(game.score(), 30);
}

TEST_F(BowlingGameTests, SpareInLastFrameGetsExtraRoll)
{
    roll_many(18, 1);
    roll_spare();
    game.roll(6);

    ASSERT_EQ(game.score(), 34);
}

TEST_F(BowlingGameTests, StrikeInLastFrameGetsTwoExtraRolls)
{
    roll_many(18, 1);
    roll_strike();
    game.roll(6);
    game.roll(1);

    ASSERT_EQ(game.score(), 35);
}

TEST_F(BowlingGameTests, PerfectGameScores300)
{
    roll_many(12, BowlingGame::pins_in_frame);

    ASSERT_EQ(game.score(), 300);
}

//////////////////////////
// Parametrized tests

struct BowlingTestParams
{
    std::initializer_list<unsigned int> pins;
    unsigned int expected_score;
};

struct BowlingBulkTests : ::testing::TestWithParam<BowlingTestParams> {
};

TEST_P(BowlingBulkTests, GameScore) {
    BowlingGame game;

    auto param = GetParam();

    for (const auto& p : param.pins)
        game.roll(p);

    ASSERT_EQ(game.score(), param.expected_score);
}

BowlingTestParams bowling_test_input[] = {
    {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 20},
    {{10, 3, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, 44},
    {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 10, 5, 5}, 38},
    {{1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 1, 9, 10}, 119},
    {{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}, 300}
};

INSTANTIATE_TEST_CASE_P(PackOfBowlingTests, BowlingBulkTests, ::testing::ValuesIn(bowling_test_input));