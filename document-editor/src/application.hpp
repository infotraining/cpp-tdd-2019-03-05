#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <unordered_map>

#include "console.hpp"
#include "command.hpp"

class Application
{
    Console& console_;
    std::unordered_map<std::string, std::shared_ptr<Command>> cmds_;
public:
    static constexpr const char* cmd_exit = "exit";

    Application(Console& console)
        : console_{console}
    {}

    void add_command(const std::string& name, std::shared_ptr<Command> cmd)
    {
        cmds_.insert(std::pair(name, cmd));
    }

    void run()
    {
        std::string cmd{};
        
        while(true)
        {
            console_.print("Enter cmd:");
            
            cmd = console_.get_line();        

            if (cmd == cmd_exit)
                break;

            if (auto pos = cmds_.find(cmd); pos != cmds_.end())
            {
                pos->second->execute();
            }
            else
            {
                console_.print("Unknown command...");
            }
            
        }
    }
};

#endif // APPLICATION_HPP
