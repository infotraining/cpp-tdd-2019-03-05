#include "document.hpp"
#include "command.hpp"
#include "mocks/mock_command.hpp"
#include "mocks/mock_console.hpp"

#include "gtest/gtest.h"

struct CommandTests : ::testing::Test
{
    Document doc{"abc"};
    MockConsole mq_console;
};

struct PrintCmd_Execute : CommandTests
{};

TEST_F(PrintCmd_Execute, PrintsDocumentInConsole)
{
    PrintCmd print_cmd{doc, mq_console};

    EXPECT_CALL(mq_console, print("[abc]"));

    print_cmd.execute();
}
