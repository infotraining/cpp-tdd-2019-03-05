#ifndef MOCK_COMMAND_HPP
#define MOCK_COMMAND_HPP

#include "gmock/gmock.h"
#include "command.hpp"

struct MockCommand : Command
{
    MOCK_METHOD0(execute, void());
};


#endif // MOCK_COMMAND_HPP
