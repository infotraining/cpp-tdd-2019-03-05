# CMake generated Testfile for 
# Source directory: /mnt/d/infotraining/trainings/cpp/cpp-tdd-2019-03-05/document-editor
# Build directory: /mnt/d/infotraining/trainings/cpp/cpp-tdd-2019-03-05/document-editor/build-wsl
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("src")
subdirs("gtests")
