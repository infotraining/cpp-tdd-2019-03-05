#include <iostream>

#include "application.hpp"
#include "command.hpp"
#include <boost/di.hpp>

using namespace std;
namespace di = boost::di;

int main()
{
    auto injector = di::make_injector(
        di::bind<Console>().to<Terminal>()        
    );

    auto app = injector.create<Application>();

    app.add_command("print", injector.create<shared_ptr<PrintCmd>>());

    app.run();
}
