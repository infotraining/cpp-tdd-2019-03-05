#include <array>
#include <iostream>
#include <limits>

// int main()
// {
//     std::array<int, 16> arr = {};

//     // arr[16] = 13;

//     // int x = std::numeric_limits<int>::max();
//     // ++x; // signed int overflow

//     {
//         void* ptr = malloc(1024);

//         std::cout << ptr << std::endl;
//     }

//     return 0;
// }

#include <stdlib.h>

int main(int argc, char** argv)
{
   void* buffer = malloc(1024);
   char* char_buffer = reinterpret_cast<char*>(buffer);
   sprintf(char_buffer, "%d", argc);
   printf("%s",char_buffer);
}