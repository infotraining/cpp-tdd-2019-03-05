#include "source.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <string>

using namespace std;

struct Console
{
    virtual std::string get_line() const = 0;
    virtual void print(const std::string& txt, int lines) = 0;
    virtual int get_width(int line) const = 0;
    virtual ~Console() {}
};

struct MockConsole : Console
{
    MOCK_CONST_METHOD0(get_line, std::string());
    MOCK_METHOD2(print, void (const std::string&, int));
    MOCK_CONST_METHOD1(get_width, int(int));
    MOCK_CONST_METHOD1(use, void(int*));
};


using namespace ::testing;

struct MockTests : Test
{
    NiceMock<MockConsole> mq_console;
};

TEST_F(MockTests, NiceMock)
{
    auto result = mq_console.get_line();

    ASSERT_EQ(result, ""s);
}

TEST_F(MockTests, OnCallSetsDefaultValue)
{
    ON_CALL(mq_console, get_line()).WillByDefault(Return("line"));

    auto result = mq_console.get_line();

    ASSERT_EQ(result, "line");
}

struct MockWithDefaultValues : MockTests
{
    void SetUp() override
    {
        ::testing::DefaultValue<string>::Set("LINE1");
    }

    void TearDown() override
    {
        ::testing::DefaultValue<string>::Clear();
    }
};

TEST_F(MockWithDefaultValues, DefaultValueSetUp)
{
    auto result = mq_console.get_line();

    ASSERT_EQ(result, "LINE1");
}

TEST_F(MockTests, VerifyHowManyTimeMethodWasCalled)
{
    EXPECT_CALL(mq_console, print("line1", Ge(1))).Times(2);

    mq_console.print("line1", 1);
    mq_console.print("line1", 1);
}


TEST_F(MockTests, VerifyHowManyTimesMethodWasCalled)
{
    EXPECT_CALL(mq_console, print("line1", Ge(1))).Times(2);

    mq_console.print("line1", 1);
    mq_console.print("line1", 1);

    EXPECT_CALL(mq_console, get_line()).WillRepeatedly(Return(""));
    EXPECT_CALL(mq_console, get_line()).Times(2).WillRepeatedly(Return("line")).RetiresOnSaturation();

    ASSERT_EQ(mq_console.get_line(), "line"s);
    ASSERT_EQ(mq_console.get_line(), "line"s);
    ASSERT_EQ(mq_console.get_line(), ""s);
}



TEST_F(MockTests, ReturnDifferentValuesBasedOnArgs)
{
    EXPECT_CALL(mq_console, get_width(AllOf(Ge(0), Lt(5)))).WillRepeatedly(Return(80));
    EXPECT_CALL(mq_console, get_width(Lt(0))).WillRepeatedly(Return(20));

    ASSERT_EQ(mq_console.get_width(3), 80);
    ASSERT_EQ(mq_console.get_width(-2), 20);
}

TEST_F(MockTests, ThrowingAnException)
{
    InSequence s;
    EXPECT_CALL(mq_console, get_width(Ge(100))).WillOnce(Throw(invalid_argument{"error"}));
    EXPECT_CALL(mq_console, print("Error", 1)).Times(1);

    ASSERT_THROW(mq_console.get_width(200), invalid_argument);
    mq_console.print("Error", 1);
}

TEST_F(MockTests, SpyingOnParams)
{
    vector<int> spy_lines;
    vector<int> results{ 10, 20, 30, 40 };
    int i = 0;

    auto first_ll = [](){ cout << "First" << endl; };

    EXPECT_CALL(mq_console, get_width(_))
        .WillRepeatedly(Invoke([&](int arg) { 
            first_ll();
            spy_lines.push_back(arg); return results[i++];
        }));

    ASSERT_EQ(mq_console.get_width(1), 10);
    ASSERT_EQ(mq_console.get_width(2), 20);
    ASSERT_EQ(mq_console.get_width(3), 30);
    ASSERT_EQ(mq_console.get_width(4), 40);        

    ASSERT_THAT(spy_lines, ElementsAre(1, 2, 3, 4));
}

TEST_F(MockTests, PointerMatchers)
{
    int* ptr = nullptr;
    int x = 10;

    EXPECT_CALL(mq_console, use(AllOf(NotNull(), Pointee(10))));
    mq_console.use(&x);
}
