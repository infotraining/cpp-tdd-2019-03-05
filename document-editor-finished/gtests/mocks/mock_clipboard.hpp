#ifndef MOCK_CLIPBOARD_HPP

#include "gmock/gmock.h"

#include "clipboard.hpp"

struct MockClipboard : Clipboard
{
    MOCK_CONST_METHOD0(content, std::string());
    MOCK_METHOD1(set_content, void (const std::string&));
};


#define MOCK_CLIPBOARD_HPP

#endif //CATCH_WITH_TROMPELOEIL_MOCK_CLIPBOARD_HPP
