#include "gmock/gmock.h"

#include "application.hpp"
#include "mocks/mock_console.hpp"
#include "mocks/mock_command.hpp"

using namespace ::testing;


class ApplicationTests : public Test
{
protected:    
    NiceMock<MockConsole> mq_console;
	std::shared_ptr<MockCommand> mq_command;
    Application app;
    

    constexpr static auto cmd_name = "TEST";
    constexpr static auto cmd_exit = "EXIT";

public:
    ApplicationTests() : mq_command(std::make_shared<MockCommand>()), app(mq_console)
    {
        app.add_command(cmd_name, mq_command);		
    }

protected:
	void SetUp() override
	{
		ON_CALL(mq_console, print(_)).WillByDefault(Return());
	}
};

struct ApplicationTests_MainLoop : ApplicationTests
{};

TEST_F(ApplicationTests_MainLoop, GetsLineFromInput)
{
    EXPECT_CALL(mq_console, get_line()).WillOnce(Return(cmd_exit));

    app.run();
}

TEST_F(ApplicationTests_MainLoop, ExecutesCommand)
{
    Sequence s;

    EXPECT_CALL(mq_console, get_line()).InSequence(s).WillOnce(Return(cmd_name));
    EXPECT_CALL(*mq_command, execute()).Times(1).InSequence(s);
    EXPECT_CALL(mq_console, get_line()).InSequence(s).WillOnce(Return(cmd_exit));

    app.run();
}

TEST_F(ApplicationTests_MainLoop, ExitEndsALoop)
{
	InSequence s;

    EXPECT_CALL(mq_console, get_line()).WillOnce(Return(cmd_exit));

    EXPECT_CALL(*mq_command, execute()).Times(0);

    app.run();
}

