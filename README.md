# cpp-tdd

## Docs

* https://infotraining.bitbucket.io/cpp-tdd/


## Libraries

* https://github.com/Microsoft/vcpkg
* https://github.com/catchorg/Catch2
* https://github.com/google/googletest
* https://boost-experimental.github.io/di/
* https://github.com/cpp-testing/GUnit

## Ankieta

* https://docs.google.com/forms/d/e/1FAIpQLScg8YzKEV6ay0qoZ2khLDkHHQv6ZVhTq6f7bxDX34NA-12KHw/viewform?hl=pl